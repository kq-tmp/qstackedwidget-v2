#include "page1.h"
#include <QDebug>

Page1::Page1()
{
    MainLayout = new QVBoxLayout(this);
    MainLayout->setAlignment(Qt::AlignTop);

    Label = new QLabel(this);
    Label->setText(QString(tr("Połączenie z bazą")));
    Label->setAlignment(Qt::AlignCenter);

    Layout = new QHBoxLayout();
    Layout->setAlignment(Qt::AlignTop);

    Spacer = new QSpacerItem(0,10);

    Label2 = new QLabel(this);
    Label2->setText(QString(tr("Typ połączenia")));

    ComboBox = new QComboBox(this);
    ComboBox->addItem(QString(tr("Komputer lokalny")));
    ComboBox->addItem(QString(tr("Komputer zdalny")));
    ComboBox->setCurrentIndex(0);

    Layout2 = new QHBoxLayout();

    Label3 = new QLabel(this);
    Label3->setText(QString(tr("Adres IP")));

    RegexIP.setPattern(QString(""));
    ValidateIP = new QRegularExpressionValidator(RegexIP, this);

    IPDatabase = new QLineEdit(this);
    IPDatabase->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
//    IPDatabase->setText("127.0.0.1");

    IPv4Database = new QHostAddress();
    IPv4Database->setAddress(IPDatabase->text());

    MainLayout->addWidget(Label);
    MainLayout->addItem(Spacer);

    Layout->addWidget(Label2);
    Layout->addWidget(ComboBox);

    Layout2->addWidget(Label3);
    Layout2->addWidget(IPDatabase);

    MainLayout->addLayout(Layout);
    MainLayout->addLayout(Layout2);

    QObject::connect(IPDatabase, &QLineEdit::textChanged, this, &Page1::setIPAdress);
}

void Page1::setSettings(AllSettings &settings)
{
    this->networkSettings = &settings;
}

void Page1::setIPAdress(const QString &ipAddress)
{
    QJsonObject ip;

    ip["ipaddr"] = ipAddress;

//    qDebug()<< ip.value("ipaddr").toString();

    emit ipAddr(ip);
}
