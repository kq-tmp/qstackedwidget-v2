#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)/*, ui(new Ui::MainWindow)*/
{
//    ui->setupUi(this);
    this->resize(600,250);

    MainWidget = new QWidget();
    MainLayout = new QVBoxLayout(MainWidget);

    FontOptions = new QFont();
    FontOptions->setWeight(QFont::Bold);
    FontOptions->setPointSize(10);

    BrushFont = new QBrush(QColor(Qt::blue));
    BrushFont->setStyle(Qt::SolidPattern);

    BrushFont2 = new QBrush(QColor(Qt::white));
    BrushFont2->setStyle(Qt::SolidPattern);

    FontColor = new QPalette();
//    FontColor->setBrush(QPalette::Active, QPalette::WindowText, *BrushFont);
    FontColor->setBrush(QPalette::Active, QPalette::Window, *BrushFont2);
//    FontColor->setColor(foregroundRole(), Qt::blue);

    Label1 = new QLabel();
    Label1->setFont(*FontOptions);
    Label1->setPalette(*FontColor);
    Label1->setLineWidth(1);
    Label1->setMidLineWidth(0);
    Label1->setFrameStyle(QFrame::Box | QFrame::Plain);
    Label1->setText(QString(tr("Opcje")));
    Label1->setAlignment(Qt::AlignCenter);
    Label1->setAutoFillBackground(true);

    Label2 = new QLabel();
    Label2->setFont(*FontOptions);
    Label2->setPalette(*FontColor);
    Label2->setLineWidth(1);
    Label2->setMidLineWidth(0);
    Label2->setFrameStyle(QFrame::Box | QFrame::Plain);
    Label2->setText(QString(tr("Ustawienia")));
    Label2->setAlignment(Qt::AlignCenter);
    Label2->setAutoFillBackground(true);

    LayoutForLabel = new QHBoxLayout();
    LayoutForLabel->addWidget(Label1);
    LayoutForLabel->addWidget(Label2);

    LayoutForStackedWidget = new QHBoxLayout();

    MyPage1 = new Page1();
    MyPage2 = new Page2();

    OptionList = new QListWidget(this);
    OptionList->addItem("Sieć");
    OptionList->addItem("Dane");
    OptionList->setCurrentRow(0);

    StackedWidget = new QStackedWidget();
    StackedWidget->addWidget(MyPage1);
    StackedWidget->addWidget(MyPage2);

    LayoutForButton = new QHBoxLayout();

    PbSave = new QPushButton(this);
    PbSave->setText(QString(tr("Zapisz ustawienia")));

    PbExit = new QPushButton(this);
    PbExit->setText(QString(tr("Zamknij")));

    mySettings.CreateSettings(myJsonObject);

//    MyPage1->setSettings(mySettings);

    mySettingsFile.setFileSettingsToSave(mySettings);

    LayoutForButton->addWidget(PbSave);
    LayoutForButton->addWidget(PbExit);

    LayoutForStackedWidget->addWidget(OptionList);
    LayoutForStackedWidget->addWidget(StackedWidget);

    MainLayout->addLayout(LayoutForLabel);
    MainLayout->addLayout(LayoutForStackedWidget);
    MainLayout->addLayout(LayoutForButton);

    this->setCentralWidget(MainWidget);

    QObject::connect(OptionList, &QListWidget::currentRowChanged, StackedWidget, &QStackedWidget::setCurrentIndex);
    QObject::connect(PbExit, &QPushButton::clicked, this, &MainWindow::close);

    QObject::connect(MyPage1, &Page1::ipAddr, &mySettings, &AllSettings::CreateSettings);

    QObject::connect(PbSave, &QPushButton::clicked, &mySettingsFile, &SettingsFile::SaveSettings);
    QObject::connect(PbSave, &QPushButton::clicked, &mySettings, &AllSettings::CrSettings);
}

MainWindow::~MainWindow()
{
//    delete mySettings;
//    delete myJsonObject;
//    delete mySettingsFile;
//    delete ui;
}

