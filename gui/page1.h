#ifndef PAGE1_H
#define PAGE1_H

#include <QObject>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QSpacerItem>
#include <QHostAddress>
#include <QLineEdit>
#include <QJsonObject>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QValidator>

#include "engine/allsettings.h"

#include <QDebug>

class Page1 : public QWidget
{
    Q_OBJECT

public:
    Page1();

    void setSettings(AllSettings &);

private slots:
    void setIPAdress(const QString &);

private:
    QVBoxLayout *MainLayout;
    QHBoxLayout *Layout, *Layout2;
    QLabel *Label, *Label2, *Label3;
    QComboBox *ComboBox;
    QSpacerItem *Spacer;
    QHostAddress *IPv4Database;
    QLineEdit *IPDatabase;
    QString IPv4;
    AllSettings *networkSettings;

    QRegularExpression RegexIP;
    QRegularExpressionMatch MatchIP;
    QValidator *ValidateIP;

signals:
    void ipAddr(QJsonObject &ip);

};

#endif // PAGE1_H
