#include "page2.h"

Page2::Page2()
{
    MainLayout = new QVBoxLayout(this);
    MainLayout->setAlignment(Qt::AlignTop);

    Label = new QLabel(this);
    Label->setText(QString(tr("Pobierz dane z")));
    Label->setAlignment(Qt::AlignCenter);

    Layout = new QHBoxLayout();

    Label2 = new QLabel(this);
    Label2->setText(QString(tr("Wydział")));

    Spacer = new QSpacerItem(0,10);

    ComboBox = new QComboBox(this);
    ComboBox->addItem("1 Cywilny");
    ComboBox->addItem("2 Cywilny");
    ComboBox->addItem("3 Karny");
    ComboBox->addItem("4 Karny");

    Layout->addWidget(Label2);
    Layout->addWidget(ComboBox);

    MainLayout->addWidget(Label);
    MainLayout->addItem(Spacer);
    MainLayout->addLayout(Layout);
}
