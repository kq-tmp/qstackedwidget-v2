#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QStackedWidget>
#include <QListWidget>
#include <QLabel>
#include <QFont>
#include <QPalette>
#include <QBrush>
#include <QPushButton>
#include <QJsonDocument>

#include "page1.h"
#include "page2.h"

#include "engine/allsettings.h"
#include "engine/fileconfig.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QHBoxLayout *LayoutForLabel, *LayoutForButton;
    QLabel *Label1, *Label2;
    QFont *FontOptions;
    QPalette *FontColor, *FontColor2;
    QBrush *BrushFont, *BrushFont2;

    QWidget *MainWidget;
    QVBoxLayout *MainLayout;
    QStackedWidget *StackedWidget;
    QListWidget *OptionList;
    QPushButton *PbSave, *PbExit;

    QHBoxLayout *LayoutForStackedWidget;

    Page1 *MyPage1;
    Page2 *MyPage2;

    AllSettings mySettings;
    QJsonObject myJsonObject;
    SettingsFile mySettingsFile;
//    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
