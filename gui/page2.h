#ifndef PAGE2_H
#define PAGE2_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QComboBox>

class Page2 : public QWidget
{
    Q_OBJECT

public:
    Page2();

private:
    QVBoxLayout *MainLayout;
    QHBoxLayout *Layout, *Layout2;
    QLabel *Label, *Label2;
    QSpacerItem *Spacer;
    QComboBox *ComboBox;
};

#endif // PAGE2_H
