#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QJsonObject>
#include <QDebug>

class AllSettings : public QObject
{
    Q_OBJECT

public:
    AllSettings();
    AllSettings(AllSettings &_settings);
    AllSettings &operator=(AllSettings &_settings);

    QJsonObject ShowSettings();
    QJsonObject *Settings();

    QString IPAddress();
    QString ConnectType();
    QString Dept();

public slots:
    void CreateSettings(QJsonObject &JsonObject);
    void CrSettings();
    void setIPAddress(const QString &_ipAddress);
    void setConnectType(const QString &_connectType);
    void setDept(const QString &_dept);

private:
    QJsonObject *createSettings;
    QString ipv4Address, connectionType, dept;
};

#endif // SETTINGS_H
