#ifndef SETTINGSFILE_H
#define SETTINGSFILE_H

#include <QObject>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QMessageBox>

#include <engine/allsettings.h>

class SettingsFile : public QObject
{
    Q_OBJECT

public:
    SettingsFile();

    void setFileSettingsToSave(AllSettings &);
    AllSettings FileSettings();
    void SaveSettings();
    void ReadSettings();

private:
    AllSettings Settings;
    QJsonObject JsonObject;
    QFile SaveSettingsToFile;
    QMessageBox Message;
};

#endif // SETTINGSFILE_H
