#include "fileconfig.h"
#include <QDebug>

SettingsFile::SettingsFile()
{

}

void SettingsFile::setFileSettingsToSave(AllSettings &Settings)
{
    this->Settings = Settings;
}

AllSettings SettingsFile::FileSettings()
{
    return Settings;
}

void SettingsFile::SaveSettings()
{
    QJsonDocument JsonDocument(JsonObject);

    SaveSettingsToFile.setFileName(QString("settings.json"));

    if(!SaveSettingsToFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)){
        Message.critical(nullptr, QString(tr("Zapis ustawień")), QString(tr("Nie można utworzyć bufora zapisu pliku")), QMessageBox::Ok);
    }
    else{
        SaveSettingsToFile.write(JsonDocument.toJson());
        SaveSettingsToFile.flush();
        SaveSettingsToFile.close();

        Message.information(nullptr, QString(tr("Zapis ustawień")), QString(tr("Plik z ustawieniami został zapisany")), QMessageBox::Ok);
    }
}
